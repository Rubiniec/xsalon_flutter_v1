import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:animator/animator.dart';
import 'package:query_params/query_params.dart';
import 'package:xsalon/customviews/progress_dialog.dart';
import 'package:xsalon/models/StdResponse.dart';
import 'package:xsalon/models/SysUser.dart';
import 'package:xsalon/screens/dialogs/dialogs.dart';
import 'package:xsalon/screens/home/home.dart';
import 'package:xsalon/utils/app_shared_preferences.dart';
import 'package:xsalon/utils/constants.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import 'package:http/http.dart' as http;
import 'package:query_params/query_params.dart';

class MatLoginPage extends StatefulWidget {
  const MatLoginPage({super.key});

  @override
  _MatLoginPageState createState() => _MatLoginPageState();
}

class _MatLoginPageState extends State<MatLoginPage> {
  ScrollController _scrollController;
  bool scrolledToDown = false;
  IconData icon = Icons.arrow_upward;
  ProgressDialog progressDialog =
      ProgressDialog.getProgressDialog(ProgressDialogTitles.USER_LOG_IN);
  final _emailTextController = TextEditingController();
  final _passwordTextController = TextEditingController();

  final _regEmailTextController = TextEditingController();
  final _regPasswordTextController = TextEditingController();
  final _regPassword2TextController = TextEditingController();

  bool bValidRegEmail = false;
  String strRegEmail = '';
  String strRegPassword = '';
  String strRegPassword2 = '';
  int bValidRegPassowrds = 0;
  bool bEnableRegister = false;
  bool bPasswordsEqual = false;
  String fcm_token;

  _checkEmailValue() {
    setState(() {
      bValidRegEmail = isValidEmail(_regEmailTextController.text);
      bEnableRegister = _getRegisterEnable();
      strRegEmail = _regEmailTextController.text;
    });
  }

  _checkPasswordValue() {
    strRegPassword = _regPasswordTextController.text;
    setState(() {
      bPasswordsEqual =
          strRegPassword == strRegPassword2 && strRegPassword2.length > 5;
      bEnableRegister = _getRegisterEnable();
    });
  }

  _checkPassword2Value() {
    strRegPassword2 = _regPassword2TextController.text;
    setState(() {
      bEnableRegister = _getRegisterEnable();
    });
  }

  _getRegisterEnable() {
    return bValidRegEmail &&
        strRegPassword == strRegPassword2 &&
        strRegPassword2.length > 5;
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    _regEmailTextController.dispose();
    super.dispose();
  }

  Future<void> readToken() async {
    var s = await AppSharedPreferences.getToken();
    setState(() {
      fcm_token = s;
      print('odczytano token');
    });
  }

  bool isValidEmail(String input) {
    final RegExp regex = RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    return regex.hasMatch(input);
  }

  Future<StdResponse> fetchLogin() async {
    //final response = await http.get('http://hisoft.pl/xapi/user/log/?key=123&password=haslo&email=w@wp.pl');

    URLQueryParams queryParams = URLQueryParams();

    queryParams.append('email', 'w@wp.pl');
    queryParams.append('password', 'haslo');
    queryParams.append('key', SysyemPreferences.KEY);

    String url = ApiEndpoints.DOMAIN + ApiEndpoints.USER_LOGIN + queryParams.toString();
    print(url);
    final response = await http.get(url);
    print(response.body);

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return StdResponse.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<StdResponse> fetchRegister() async {
    //final response = await http.get('http://hisoft.pl/xapi/user/log/?key=123&password=haslo&email=w@wp.pl');
    // http://hisoft.pl/xapi/user/new/?email=whade1@wp.pl&key=123&token=hduiwehd&tel=2738279897&password=haslo
    URLQueryParams queryParams = URLQueryParams();

    queryParams.append('email', strRegEmail);
    queryParams.append('token', fcm_token);
    queryParams.append('tel', 'haslo');
    queryParams.append('password', strRegPassword);
    queryParams.append('key', SysyemPreferences.KEY);

    String url = ApiEndpoints.DOMAIN + ApiEndpoints.USER_REGISTER + queryParams.toString();
    print(url);
    final response = await http.get(url);
    print(response.body);

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return StdResponse.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  void logout() async {
    setState(() {
      AppSharedPreferences.setUserLoggedIn(false);

      // _goToHomeScreen();
    });
  }

  Future _goToHomeScreen() async {
    bool isLoggedIn = await AppSharedPreferences.isUserLoggedIn();
    if (isLoggedIn == true) {
      log('jest ');
    } else {
      log('nie m a ');
    }

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => MyHomePage()),
    );
  }

  void login() async {
    log('tyestowe logowanie ');
    String email = _emailTextController.text;
    String password = _passwordTextController.text;
    //  progressDialog.showProgress();
    StdResponse a = await fetchLogin();
    //StdResponse a = new StdResponse();  //await fetchLogin();
    int k = 1;
    //log(a.id.toString());
    SysUser sysUser = SysUser();
    sysUser.email = '';
    sysUser.name = '';
    sysUser.password = 'haslo';
    sysUser.token = '';

    ///sysUser.id=0;

    if (a.id > 0)
    //if(k>0)
    {
      sysUser.id = a.id;
      setState(() {
        AppSharedPreferences.setUserLoggedIn(true);
        AppSharedPreferences.setUserProfile(sysUser);
        // AppSharedPreferences.setUserProfile(eventObject.object);

        // progressDialog.hideProgress();
        _goToHomeScreen();
      });
    } else {
      sysUser.password = '';
      AppSharedPreferences.setUserLoggedIn(false);
      AppSharedPreferences.setUserProfile(sysUser);
      //  progressDialog.hideProgress();
      showAlert(a.status);
      //  _goToHomeScreen();
    }
  }

  void register() async {
    print('==>> REGISTER');
    String email = _emailTextController.text;
    String password = _passwordTextController.text;
    //  progressDialog.showProgress();
    StdResponse a = await fetchRegister();
    //StdResponse a = new StdResponse();  //await fetchLogin();
    int k = 1;
    //log(a.id.toString());
    SysUser sysUser = SysUser();
    sysUser.email = '';
    sysUser.name = '';
    sysUser.password = 'haslo';
    sysUser.token = '';
    
    ///sysUser.id=0;

    if (a.id > 0)
    //if(k>0)
    {
      sysUser.id = a.id;
      
      setState(() {
        AppSharedPreferences.setUserLoggedIn(true);
        AppSharedPreferences.setUserProfile(sysUser);
        // AppSharedPreferences.setUserProfile(eventObject.object);

        // progressDialog.hideProgress();
      
       // _goToHomeScreen();
      });
    } else {
      sysUser.password = '';
      AppSharedPreferences.setUserLoggedIn(false);
      AppSharedPreferences.setUserProfile(sysUser);
      //  progressDialog.hideProgress();
      showAlert(a.status);
      //  _goToHomeScreen();
    }
  }

  void showAlert(int status) {
    String strAlert;
    strAlert = '';
    switch (status) {
      case 0:
        strAlert = '';
        break;
      case 3:
        strAlert = 'Użytkownik istnieje';
        break;
    }
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: const Text("Błąd"),
            // content: new Text("Alert content"),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text(strAlert),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                  }),
            ],
          );
        });
  }

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
   
  }

  @override
  initState() {
    _regEmailTextController.addListener(_checkEmailValue);
    _regPasswordTextController.addListener(_checkPasswordValue);
    _regPassword2TextController.addListener(_checkPassword2Value);
    readToken();
    print(fcm_token);
    super.initState();

    _scrollController = ScrollController()
      ..addListener(() {
        // print("offset = ${_scrollController.offset}");

        if (_scrollController.offset ==
            _scrollController.position.maxScrollExtent) {
          setState(() {
            scrolledToDown = !scrolledToDown;
            icon = Icons.arrow_downward;
          });
        } else if (_scrollController.offset ==
            _scrollController.position.minScrollExtent) {
          setState(() {
            scrolledToDown = !scrolledToDown;
            icon = Icons.arrow_upward;
          });
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        decoration: BoxDecoration(
            image: const DecorationImage(
                fit: BoxFit.cover,
                image: const AssetImage('assets/images/loginScreen_bg.png'))),
        child: ListView(
          controller: _scrollController,
          scrollDirection: Axis.vertical,
          children: <Widget>[
            Stack(
              children: <Widget>[
                 IconButton(
             icon: const Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop(null),
           ),        IconButton(
             icon: const Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop(null),
           ),
                Container(
                  
                  
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).size.height / 7,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: const AssetImage('assets/images/login_bg.png'),
                    ),
                  ),
                  
                  child: Stack(
                    children: <Widget>[
                      _buildLoginContent(),
                      _buildSocialButtons(),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).size.height / 7,
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height -
                          MediaQuery.of(context).size.height / 4),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: const AssetImage('assets/images/register_bg.png'),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      _setUpDownToggleButton(),
                      _buildRegisterContent(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginContent() {
    return Container(
      padding: const EdgeInsets.only(left: 50.0, right: 50.0),
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 10),
      child: Column(
        children: <Widget>[

   Padding(
            padding: const EdgeInsets.only(left: 10),
            child:      IconButton(
             icon: const Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop(null),
          
            ),
          ),

     
          TextFormField(
            style: const TextStyle(fontSize: 14),
            decoration: InputDecoration(
              alignLabelWithHint: true,
              suffixIcon: const Icon(
                Icons.email,
                color: Colors.grey,
                size: 20.0,
              ),
              hintText: "Email",
              hintStyle: const TextStyle(color: Colors.grey, fontSize: 18.0),
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _emailTextController,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: TextFormField(
              style: const TextStyle(fontSize: 14),
              decoration: InputDecoration(
                alignLabelWithHint: true,
                suffixIcon: Icon(
                  Icons.lock,
                  color: Colors.grey,
                  size: 20.0,
                ),
                hintText: "Password",
                hintStyle: const TextStyle(color: Colors.grey, fontSize: 18.0),
              ),
              keyboardType: TextInputType.emailAddress,
              controller: _passwordTextController,
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: Text(
                'Forget Password?',
                style: const TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey,
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.end,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () {
                login();
                // Navigator.of(context).pushNamed(HomePage.tag);
              },
              child: Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 10.0),
                width: 180.0,
                height: 40.0,
                decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    colors: const <Color>[Color(0xFFfc6960), Color(0xFFf95887)],
                  ),
                  borderRadius:
                      const BorderRadius.all(const Radius.circular(30.0)),
                ),
                child: Text(
                  "LOGIN",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 22.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRegisterContent() {
    return Container(
      padding: const EdgeInsets.only(left: 50.0, right: 50.0),
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 7),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 1.0),
            child: TextFormField(
              style: const TextStyle(fontSize: 14),
              decoration: InputDecoration(
                alignLabelWithHint: true,
                suffixIcon: const Icon(
                  Icons.email,
                  color: Colors.grey,
                  size: 20.0,
                ),
                hintText: "Email",
                hintStyle: const TextStyle(color: Colors.grey, fontSize: 18.0),
              ),
              keyboardType: TextInputType.emailAddress,
              controller: _regEmailTextController,
              validator: (value) => isValidEmail(value)
                  ? null
                  : 'Please enter a valid email address',
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: TextFormField(
              obscureText: true,
              style: const TextStyle(fontSize: 14),
              decoration: InputDecoration(
                alignLabelWithHint: true,
                suffixIcon: Icon(
                  Icons.lock,
                  color: bPasswordsEqual ? Colors.grey : Colors.red,
                  size: 20.0,
                ),
                hintText: "Password",
                hintStyle: const TextStyle(color: Colors.grey, fontSize: 18.0),
              ),
              keyboardType: TextInputType.emailAddress,
              controller: _regPasswordTextController,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: TextFormField(
              obscureText: true,
              style: const TextStyle(fontSize: 14),
              decoration: InputDecoration(
                alignLabelWithHint: true,
                suffixIcon: const Icon(
                  Icons.lock,
                  color: Colors.grey,
                  size: 20.0,
                ),
                hintText: "Confirm Password",
                hintStyle: const TextStyle(color: Colors.grey, fontSize: 18.0),
              ),
              keyboardType: TextInputType.emailAddress,
              controller: _regPassword2TextController,
            ),
          ),
          Container(
            alignment: Alignment.topRight,
            child: Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Text(
                'Already Registered?',
                style: const TextStyle(
                    fontSize: 18.0,
                    color: const Color(0xFFf95887),
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.end,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: InkWell(
              onTap: () {
                
                register();
                showXDialog(context);
              },
              child: Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 20.0),
                width: 180.0,
                height: 45.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: bEnableRegister
                        ? (<Color>[const Color(0xFFfc6960), const Color(0xFFf95887)])
                        : (<Color>[const Color(0xFFA9a9a9), const Color(0xFFeeeeee)]),
                  ),
                  borderRadius:
                      BorderRadius.all(const Radius.circular(30.0)),
                ),
                child: Text(
                  "REGISTER",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setUpDownToggleButton() {
    return Align(
      alignment: Alignment.topLeft,
      child: Animator(
        tween: Tween<double>(begin: 0.8, end: 1.0),
        curve: Curves.fastOutSlowIn,
        cycles: 0,
        builder: (anim) => Transform.scale(
          scale: anim.value,
          child: Container(
            height: 65.0,
            width: 65.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(const Radius.circular(40.0)),
                boxShadow: [
                  const BoxShadow(
                      color: const Color(0xFFfc6960),
                      blurRadius: 20.0,
                      spreadRadius: 1.0,
                      offset: const Offset(0.6, 2.0)),
                ]),
            alignment: Alignment.topLeft,
            margin: const EdgeInsets.only(top: 50.0, left: 35.0),
            child: ClipOval(
              child: Container(
                color: const Color(0xFFf958FF),
                height: 65.0, // height of the button
                width: 65.0, // width of the button
                child: Center(
                    child: InkWell(
                  onTap: () {
                    setState(() {
                      if (!scrolledToDown) {
                        icon = Icons.arrow_downward;
                        _scrollToBottom();
                      } else {
                        icon = Icons.arrow_upward;
                        _scrollToTop();
                      }
                    });
                  },
                  child: Icon(
                    icon,
                    size: 38.0,
                    color: Colors.white,
                  ),
                )),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _scrollToBottom() {
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        curve: Curves.linear, duration: const Duration(milliseconds: 500));
  }

  void _scrollToTop() {
    _scrollController.animateTo(_scrollController.position.minScrollExtent,
        curve: Curves.linear, duration: const Duration(milliseconds: 500));
  }

  //---------------------------------FaceBookLogin--------------------------------------------------
  static final FacebookLogin facebookSignIn = FacebookLogin();

  Future<FirebaseUser> _signInFaceBook(BuildContext context) async {
    final facebookLoginResult = await facebookSignIn
        .logInWithReadPermissions(['email', 'public_profile']);
    FacebookAccessToken myToken = facebookLoginResult.accessToken;
    AuthCredential credential =
        FacebookAuthProvider.getCredential(accessToken: myToken.token);
    FirebaseUser user =
        await FirebaseAuth.instance.signInWithCredential(credential);

    ProviderDetails userInfo = ProviderDetails(
        user.providerId, user.uid, user.displayName, user.photoUrl, user.email);

    List<ProviderDetails> providerData = List<ProviderDetails>();
    providerData.add(userInfo);

    UserInfoDetails userInfoDetails = UserInfoDetails(
        user.providerId,
        user.uid,
        user.displayName,
        user.photoUrl,
        user.email,
        user.isAnonymous,
        user.isEmailVerified,
        providerData);
    print(user.email);
    print(user.uid);
    print(user.providerData);
/*
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) =>
            new DetailedScreen(detailsUser: userInfoDetails, user: user),
      ),
    );
    */
    return user;
  }

  Widget _buildSocialButtons() {
    return Align(
      alignment: Alignment.bottomRight,
      child: Container(
          height: 200.0,
          margin: const EdgeInsets.only(right: 50.0, bottom: 10.0),
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () {
                  _signInFaceBook(context);
                  // Navigator.of(context).pushNamed(HomePage.tag);
                },
                child: Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(top: 10.0),
                  width: 180.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                      colors: const <Color>[Color(0xFF295093), Color(0xFF2950ff)],
                    ),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(20.0)),
                  ),
                  child: Text(
                    "facebook",
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 22.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}

class ProviderDetails {
  final String providerId;

  final String uid;

  final String displayName;

  final String photoUrl;

  final String email;

  ProviderDetails(
      this.providerId, this.uid, this.displayName, this.photoUrl, this.email);
}

class UserInfoDetails {
  UserInfoDetails(this.providerId, this.uid, this.displayName, this.photoUrl,
      this.email, this.isAnonymous, this.isEmailVerified, this.providerData);

  /// The provider identifier.
  final String providerId;

  /// The provider’s user ID for the user.
  final String uid;

  /// The name of the user.
  final String displayName;

  /// The URL of the user’s profile photo.
  final String photoUrl;

  /// The user’s email address.
  final String email;

  // Check anonymous
  final bool isAnonymous;

  //Check if email is verified
  final bool isEmailVerified;

  //Provider Data
  final List<ProviderDetails> providerData;
}

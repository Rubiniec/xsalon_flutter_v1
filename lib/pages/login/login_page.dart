import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xsalon/customviews/progress_dialog.dart';
import 'package:xsalon/models/StdResponse.dart';
import 'package:xsalon/models/SysUser.dart';
import 'package:xsalon/screens/home/home.dart';
import 'package:xsalon/utils/app_shared_preferences.dart';
import 'package:xsalon/utils/constants.dart';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:query_params/query_params.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  const LoginPage({super.key});
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();
  ProgressDialog progressDialog =
      ProgressDialog.getProgressDialog(ProgressDialogTitles.USER_LOG_IN);

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/images/logo.png'),
      ),
    );

    final emailTextController = TextEditingController();
    final passwordTextController = TextEditingController();

    @override
    void dispose() {
      log('tyestowe logowanie ');
      passwordTextController.dispose();
    }

    Future goToHomeScreen() async {
      bool isLoggedIn = await AppSharedPreferences.isUserLoggedIn();
      if (isLoggedIn == true) {
        log('jest ');
      } else {
        log('nie m a ');
      }

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => MyHomePage()),
      );
    }

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      //   initialValue: 'email',
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
      controller: emailTextController,
    );

    final password = TextFormField(
      autofocus: false,
      //  initialValue: 'password',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
      controller: passwordTextController,
    );
/*
    Future<StdResponse> fetchLogin() async {
      //final response = await http.get('http://hisoft.pl/xapi/user/log/?key=123&password=haslo&email=w@wp.pl');

      URLQueryParams queryParams = new URLQueryParams();

      queryParams.append('email', 'w@wp.pl');
      queryParams.append('password', 'haslo');
      queryParams.append('key', SysyemPreferences.KEY);

      String url = 'http://hisoft.pl/xapi/user/log/?' + queryParams.toString();
      print(url);
      final response = await http.get(url);
      print(response.body);

      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON.
        return StdResponse.fromJson(json.decode(response.body));
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Failed to load post');
      }
    }
*/
    void logout() async {
      setState(() {
        AppSharedPreferences.setUserLoggedIn(false);

        goToHomeScreen();
      });
    }

    void showAlert() {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CupertinoAlertDialog(
              title: const Text("Błąd"),
              // content: new Text("Alert content"),
              actions: [
                CupertinoDialogAction(
                    isDefaultAction: true,
                    child: const Text("Nieprawidłowe dane  logowania"),
                    onPressed: () {
                      Navigator.pop(context, 'Cancel');
                    }),
              ],
            );
          });
    }

    void login() async {
      log('tyestowe logowanie ');
      String email = emailTextController.text;
      String password = passwordTextController.text;
      progressDialog.showProgress();
      StdResponse a;//await fetchLogin();
      //StdResponse a = new StdResponse();  //await fetchLogin();
      int k = 1;
      //log(a.id.toString());
      SysUser sysUser = SysUser();
      sysUser.email = '';
      sysUser.name = '';
      sysUser.password = 'haslo';
      sysUser.token = '';

      ///sysUser.id=0;

      if (a.id > 0)
      //if(k>0)
      {
        sysUser.id = a.id;
        setState(() {
          AppSharedPreferences.setUserLoggedIn(true);
          AppSharedPreferences.setUserProfile(sysUser);
          // AppSharedPreferences.setUserProfile(eventObject.object);

          progressDialog.hideProgress();
          goToHomeScreen();
        });
      } else {
        sysUser.password = '';
        AppSharedPreferences.setUserLoggedIn(false);
        AppSharedPreferences.setUserProfile(sysUser);
        progressDialog.hideProgress();
        showAlert();
        //  _goToHomeScreen();
      }
    }

    final loginButton = Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        onPressed: () {
          login();
          // Navigator.of(context).pushNamed(HomePage.tag);
        },
        padding: const EdgeInsets.all(12),
        color: Colors.greenAccent,
        child: const Text('Zaloguj', style: TextStyle(color: Colors.white)),
      ),
    );

    final logoutButton = Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        onPressed: () {
          logout();
          // Navigator.of(context).pushNamed(HomePage.tag);
        },
        padding: const EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: const Text('Wyloguj', style: TextStyle(color: Colors.white)),
      ),
    );

    final forgotLabel = FlatButton(
      child: const Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            const SizedBox(height: 48.0),
            email,
            const SizedBox(height: 8.0),
            password,
            const SizedBox(height: 24.0),
            loginButton,
            logoutButton,
            forgotLabel,
            progressDialog
          ],
        ),
      ),
    );
  }
}

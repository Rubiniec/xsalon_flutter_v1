import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';


class MGoogleSignInAccount {
  late String displayName;
  late String email;
  late String id;
  late String photoUrl;
  late String serverAuthCode;
 // Pusty konstruktor
  MGoogleSignInAccount (){
    // TODO: implement MGoogleSignInAccount
    
  }

 @override
  String toString() {
    return 'GoogleSignInAccount {displayName: $displayName, email: $email, id: $id, photoUrl: $photoUrl, serverAuthCode: $serverAuthCode}';
  }

 
}


Future<dynamic> signInWithGoogle(MGoogleSignInAccount konto) async {
    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
  //    print("google user otrzymany z firemase");
  //  print(googleUser);
    konto.displayName = googleUser!.displayName!;
    konto.email = googleUser.email;
    konto.id = googleUser.id;
    konto.photoUrl = googleUser.photoUrl!;
    konto.serverAuthCode = googleUser.serverAuthCode!;

      final GoogleSignInAuthentication? googleAuth =
          await googleUser.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );
      final UserCredential cred = await FirebaseAuth.instance.signInWithCredential(credential);
      print('Login successful. Firebase UID: ${cred.user!.uid}');
      return cred;
    } on Exception {
      // TODO
 //     print("error: w funkcji singningoole");
  //    print('exception->$e');
    }
  }

  Future<bool> signOutFromGoogle() async {
    try {
      await FirebaseAuth.instance.signOut();
      return true;
    } on Exception catch (_) {
      return false;
    }
  }

class GoogleSignInScreen extends StatefulWidget {
  const GoogleSignInScreen({super.key});

  @override
  State<GoogleSignInScreen> createState() => _GoogleSignInScreenState();
}

class _GoogleSignInScreenState extends State<GoogleSignInScreen> {
  ValueNotifier userCredential = ValueNotifier('');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Google SignIn Screen')),
        body: ValueListenableBuilder(
            valueListenable: userCredential,
            builder: (context, value, child) {
              return (userCredential.value == '' ||
                      userCredential.value == null)
                  ? Center(
  child: Card(
    elevation: 5,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10)
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          iconSize: 10,
          icon: Image.asset(
            'lib/assets/images/google_icon_1.png',
          ),
          onPressed: googleClick,
        ),
        const SizedBox(height: 10), // Odstęp pomiędzy przyciskami
        IconButton(
          iconSize: 10,
          icon: Image.asset(
            'lib/assets/images/google_icon_1.png', // Ścieżka do obrazu kolejnego przycisku
          ),
          onPressed: anotherClick, // Funkcja obsługująca kolejny przycisk
        ),
      ],
    ),
  ),
)
                  : Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    width: 1.5, color: Colors.black54)),
                            child: Image.network(
                                userCredential.value.user!.photoURL.toString()),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(userCredential.value.user!.displayName
                              .toString()),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(userCredential.value.user!.email.toString()),
                          const SizedBox(
                            height: 30,
                          ),
                          ElevatedButton(
                              onPressed: () async {
                                bool result = await signOutFromGoogle();
                                if (result) userCredential.value = '';
                              },
                              child: const Text('Logout'))
                        ],
                      ),
                    );
            }));
  }

  Future<void> googleClick() async {
    {
                               MGoogleSignInAccount konto = MGoogleSignInAccount();
                               userCredential.value = await signInWithGoogle(konto);
                               if (userCredential.value != null)
                               {

                                print(userCredential.toString());
                                print(konto.toString());
                               }
                                 else {
                                 print("----------ERROR-----------");
                               }
                             }
  }
  Future<UserCredential> signInWithFacebook() async {
  try {
    // Trigger the sign-in flow
    final LoginResult loginResult = await FacebookAuth.instance.login();





    // Sprawdź, czy uzyskano dostęp do tokenu
    if (loginResult.status == LoginStatus.success) {
      // Create a credential from the access token
      final AccessToken accessToken = loginResult.accessToken!;
      final OAuthCredential credential = FacebookAuthProvider.credential(accessToken.token);
      print("Success");


final OAuthCredential facebookAuthCredential = FacebookAuthProvider.credential(loginResult .accessToken!.token);
FirebaseAuth auth = FirebaseAuth.instance;

     final UserCredential userCredential =
      await auth.signInWithCredential(facebookAuthCredential);

      print('Login successful. Firebase UID: ${userCredential.user!.uid}');
    return userCredential;

/*
 final UserCredential userCredential = await _auth.signInWithCredential(credential);

      print('Login successful. Firebase UID: ${userCredential.user!.uid}');



      print(credential.toString());
      // Once signed in, return the UserCredential
     // return await FirebaseAuth.instance.signInWithCredential(credential);
      return await FirebaseAuth.instance.signInWithCredential( credential );
      */
    } else {
      // W przypadku niepowodzenia, zwróć null lub obsłuż błąd w inny sposób
      throw Exception('------------------Facebook login failed------------');
    }
  } catch (e) {
    // Obsłuż wyjątek
    print('Error signing in with Facebook: $e');
    rethrow; // Rzucenie wyjątku dalej dla obsługi w innych miejscach
  }
}



    anotherClick() async {
    {
       final userCredential = await signInWithFacebook();
       print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>userCredential"); 
        print(userCredential.toString());
        final idToken = userCredential.user!.getIdToken();
      }

    }



}